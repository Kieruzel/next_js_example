import type { AppProps } from "next/app";
import { useEffect } from "react";

export default function MyApp({ Component, pageProps }: AppProps) {

  return (
    <>
      <div style={{ background: "red" }}>
        <Component {...pageProps} />
      </div>
    </>
  );
}
