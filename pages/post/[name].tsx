import { useRouter } from 'next/router'
import React from 'react'

type Props = {

}

const  Name = ({} : Props) : JSX.Element => {
    const router = useRouter()

    console.log(router)


    return (
        <h1>Name: {router.query?.name}</h1>
    )
}

export default Name;