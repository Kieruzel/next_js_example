import { GetServerSideProps } from "next";
import { notFound } from "next/navigation";
import { useRouter } from "next/router";
import React from "react";

type Props = {
  property: any;
};

const Ssr = ({ property }: Props): JSX.Element => {

    const router = useRouter();


    if(router.isFallback) {
        return <p>Loading .....</p>
    }



  return (
    <section>
      <h1>{property?.post_title}</h1>
      {/* <ul>
        {properties?.map((x) => {
          return <li key={x.ID}>{x.post_title}</li>;
        })}
      </ul> */}
    </section>
  );
};

export default Ssr;

export const getStaticPaths = async () => {
  const res = await fetch(
    `https://cvrapidev.noinputsignal.com/property?site=luxurypropertydanang.com&page=1&limit=10&lang=en`
  );
  const json = await res.json();


//   console.log(json)

  const params = json.posts?.map((x) => {
    return {
      params: { id: x.ID },
    };
  });

  return {
    paths: params,
    fallback: true, // false or "blocking"
  };
};

export const getStaticProps = async (context) => {
  console.log("SSG Request has been made");

  try {
    const res = await fetch(
      `https://cvrapidev.noinputsignal.com/property/${context.params.id}`
    );
    const json = await res.json();

    // const x = () => {
    //   return new Promise((resolve) => {
    //     setTimeout(() => {
    //       resolve("X");
    //     }, 3000);
    //   });
    // };

    // await x();

    return {
      props: {
        property: json,
      },
    };
  } catch (error) {
    return {
      props: {
        property: [],
      },
    };
  }
};
