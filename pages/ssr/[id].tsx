import { GetServerSideProps } from "next";
import React from "react";

type Props = {
  property: any;
};

const Ssr = ({ property }: Props): JSX.Element => {
  return (
    <section>
        <h1>{property ? property?.post_title : ''}</h1>
      {/* <ul>
        {properties?.map((x) => {
          return <li key={x.ID}>{x.post_title}</li>;
        })}
      </ul> */}
    </section>
  );
};

export default Ssr;

export const getServerSideProps = async (context) => {
  console.log("Request has been made");


  console.log(context.query)

  try {
    const res = await fetch(
      `https://cvrapidev.noinputsignal.com/property/${context.query.id}`
    );
    const json = await res.json();

    // const x = () => {
    //   return new Promise((resolve) => {
    //     setTimeout(() => {
    //       resolve("X");
    //     }, 3000);
    //   });
    // };

    // await x();

    return {
      props: {
        property: json,
      },
    };
  } catch (error) {
    return {
      props: {
        property: [],
      },
    };
  }
};
