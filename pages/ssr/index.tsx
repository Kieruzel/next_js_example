import { GetServerSideProps } from "next";
import React from "react";

type Props = {
  properties: any;
};

const Ssr = ({ properties }: Props): JSX.Element => {
  return (
    <section>
      <ul>
        {properties?.map((x) => {
          return <li key={x.ID}>{x.post_title}</li>;
        })}
      </ul>
    </section>
  );
};

export default Ssr;

export const getServerSideProps = async (context) => {
  console.log("Request has been made");

  try {
    const res = await fetch(
      `https://cvrapidev.noinputsignal.com/property?site=luxurypropertydanang.com&page=1&limit=10&lang=en`
    );
    const json = await res.json();

    const x = () => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve("X");
        }, 3000);
      });
    };

    await x();

    return {
      props: {
        properties: json.posts,
      },
    };
  } catch (error) {
    return {
      props: {
        properties: [],
      },
    };
  }
};
